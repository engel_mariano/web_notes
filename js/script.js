import PropTypes from 'prop-types';

class Welcome extends React.Component {


    edit(){
        console.log("Editing node...");
    }

    remove(){
        console.log("Removing node...");
    }
    render() {

        return <div class="bg-red p-3">
            <b>Firstname: {this.props.firstname}</b>
            <br/>
            <b>Lastname: {this.props.lastname}</b>
            <p>{this.props.children}</p>
            <div class="button-holder">
                <button onClick={this.edit} class="btn  btn-success fas fa-pencil-alt mr-2 ml-2"/>
                <button onClick={this.remove} className="btn  btn-danger fas fa-trash-alt"/>
            </div>

        </div>;
    }

}

class Task extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            checked: false,
        };
        this.handleCheck = this.handleCheck.bind(this);//to remove this use the arrow function
    }


    handleCheck(){
        console.log(this.state.checked);
        this.setState({checked: !this.state.checked});


    }

    edit(){
        console.log("Editing node...");
    }

    remove(){
        console.log("Removing node...");
    }
    render() {

        var checkValue = (this.state.checked) ? "Checked" : "Unchecked";

        return <div class="bg-red p-3">
            <b>{this.props.title}</b>
            <p>{this.props.children}</p>
            <div class="button-holder">
                <button onClick={this.edit} class="btn  btn-success fas fa-pencil-alt mr-2 ml-2"/>
                <button onClick={this.remove} className="btn  btn-danger fas fa-trash-alt"/>
            </div>
            <div className="form-check form-check-inline">
                <input className="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" defaultChecked={this.state.checked} onChange={this.handleCheck}/>
                <label className="form-check-label" htmlFor="inlineCheckbox1">{checkValue}</label>
            </div>


        </div>;
    }

}

class Task2 extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isEditing: false
        };

        this.newTextValue = React.createRef();

    }


    edit(){
        this.setState({isEditing: true});
    }

    save(){

        var val = this.newTextValue.current.value;
        console.log(val);
        this.setState({isEditing: false});
    }

    remove(){
        console.log("Removing...");
    }


    renderDisplay(){
        return (<div class="bg-red p-3">
            <b>{this.props.title}</b>
            <p>{this.props.children}</p>
            <div class="button-holder">
                <button onClick={()=>this.edit()} class="btn  btn-success fas fa-pencil-alt mr-2 ml-2"/>
                <button onClick={()=>this.remove()} className="btn  btn-danger fas fa-trash-alt"/>

            </div>
        </div>)
    }


    renderForm(){
        return(<div class="bg-red p-3">
            <input type="text" value={this.props.title} class="form-control"/>
            <br/>
            <textarea ref={this.newTextValue} defaultValue={this.props.children} class="form-control"></textarea>

            <div class="button-holder">
                <button onClick={()=>this.save()} class="btn  btn-success fas fa-save mr-2 ml-2"/>
            </div>
        </div>)
    }

    render() {

        if(this.state.isEditing){
            return(this.renderForm());
        }else{
            return(this.renderDisplay());
        }

    }

}

ReactDOM.render(<div>
        <Task2 title="Task 2">
            The quick brown fox jumps over the head of the lazy dog.<br/>
        </Task2>

    </div>,
    document.getElementById('task2'));

ReactDOM.render(<div>
        <Task title="Task 1">
            The quick brown fox jumps over the head of the lazy dog.<br/>
        </Task>

    </div>,
    document.getElementById('task1'));




ReactDOM.render(<div>
        <Welcome firstname="Engel" lastname="Mariano">
            Senior Fullstack Developer<br/>
            Ossocraft Labs
            <p>Hell Yeah!</p>
        </Welcome>

    </div>,
    document.getElementById('name'));