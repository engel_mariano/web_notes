class Welcome extends React.Component {

    render() {

        return React.createElement("h1", null, "Hello, ", this.props.name);

    }

}

ReactDOM.render(React.createElement("div", null, 
                    React.createElement(Welcome, {name: "Engel1"}), 
                    React.createElement(Welcome, {name: "Engel sub 1"})
                )
                , document.getElementById('id1'));
ReactDOM.render(React.createElement(Welcome, {name: "Engel2"}), document.querySelector('#id2'));