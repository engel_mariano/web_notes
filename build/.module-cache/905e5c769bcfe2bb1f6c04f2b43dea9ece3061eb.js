class Welcome extends React.Component {

    render() {

        return React.createElement("h1", null, "Hello, ", this.props.name);

    }

}

ReactDOM.render(React.createElement(Welcome, {name: "Engel"}), document.body);